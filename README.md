# Terra Design System

Trimbles's Design System Framework
v 1.0.0

## Installation

To use Terra, you can quickly load the files inside the `/dist` directory into your project.

```html
<link rel="stylesheet" href="terra.css" />
```

```html
<script src="terra.js"></script>
```

Alternatively, the SASS files are included in this package, and can be added to any SASS project and compiled.
